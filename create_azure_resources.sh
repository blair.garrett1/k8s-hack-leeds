# Login
az login --service-principal --username $APP_ID --password $PASSWORD --tenant $TENANT_ID
az account set --subscription b53078e0-1398-4d0e-99b0-4e70f9c6d920

resource_group="team6ResourceGroup"$SUFFIX
# Resource Group
az group create --location $LOCATION --name $resource_group
az group lock create --lock-type CanNotDelete --name deleteLock --resource-group $resource_group

# AKS Cluster
az aks create --service-principal $APP_ID --client-secret $PASSWORD --resource-group $resource_group --name team6AKSCluster$SUFFIX --location $LOCATION --node-count 3 --kubernetes-version 1.8.7 --node-vm-size Standard_D2_v3 --node-count 5 --generate-ssh-keys
az aks get-credentials --resource-group $resource_group --name team6AKSCluster$SUFFIX

