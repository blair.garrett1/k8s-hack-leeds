az login --service-principal --username $APP_ID --password $PASSWORD --tenant $TENANT_ID

#Secondary Cluster west europe
az aks get-credentials --resource-group=team6ResourceGroup-westeurope --name=team6AKSCluster-westeurope
kubectl config use-context team6AKSCluster-westeurope

helm init
helm install --name loitering-alpaca stable/rabbitmq

# cosmos secrets
echo -n "$COSMOSURL" > mongodburl
kubectl create secret generic cosmosdb --from-file=./mongodburl

kubectl apply -f orderservice.yml
kubectl apply -f orderffulfilment_secondary.yml
kubectl apply -f eventlistener.yml 
kubectl apply -f testnamespace.yml

#Monitoring
kubectl apply -f sysdig-daemonset.yaml
