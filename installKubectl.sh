if [ ! -f /kubectl ]; then 
    wd=`pwd` && wget https://storage.googleapis.com/kubernetes-release/release/v1.6.4/bin/linux/amd64/kubectl && export PATH=$PATH:$wd && chmod +x /kubectl
else
    wd=`pwd` && export PATH=$PATH:$wd && chmod +x /kubectl
fi

# wd=`pwd` && wget https://storage.googleapis.com/kubernetes-release/release/v1.6.4/bin/linux/amd64/kubectl && export PATH=$PATH:$wd && chmod +x ./kubectl