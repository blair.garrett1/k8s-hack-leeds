az login --service-principal --username $APP_ID --password $PASSWORD --tenant $TENANT_ID
az aks get-credentials --resource-group=$RESOURCE_GROUP --name=$CLUSTER_NAME

kubectl config use-context $CLUSTER_NAME
echo -n "$COSMOSURL" > mongodburl
kubectl create secret generic cosmosdb --from-file=./mongodburl

# service catalog
# helm init
# helm repo add svc-cat https://svc-catalog-charts.storage.googleapis.com
# helm install svc-cat/catalog --name catalog --namespace catalog --set rbacEnable=false


kubectl apply -f orderservice.yml
kubectl apply -f $ORDER_FULFILMENT
kubectl apply -f eventlistener.yml 
kubectl apply -f testnamespace.yml

#Monitoring
kubectl apply -f sysdig-daemonset.yaml