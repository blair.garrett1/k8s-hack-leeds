#West Europe Cluster
az group create --location westeurope --name team6ResourceGroup-westeurope
az group lock create --lock-type CanNotDelete --name deleteLock --resource-group team6ResourceGroup-westeurope
az aks create --resource-group team6ResourceGroup-westeurope --name team6AKSCluster-westeurope --node-count 3 --kubernetes-version 1.8.7 --generate-ssh-keys -s Standard_D2_v3
az aks get-credentials --resource-group=team6ResourceGroup-westeurope --name=team6AKSCluster-westeurope
az cosmosdb create --resource-group MC_team6ResourceGroup-westeurope_team6AKSCluster-westeurope_westeurope --kind MongoDB --name team6cosmos-westeurope
az storage account create --resource-group MC_team6ResourceGroup-westeurope_team6AKSCluster-westeurope_westeurope  --name team6storaccweurope --location westeurope --sku Standard_LRS